use core::ops::Add;
use crate::Vec2;

///
/// ```
/// use gfxmath_vec2::Vec2;
/// let a = Vec2::new(1.0, 2.0);
/// let b = Vec2::new(3.0, 4.0);
/// 
/// let res = &a + &b;
/// 
/// assert_eq!(1.0, a.x);
/// assert_eq!(2.0, a.y);
/// 
/// assert_eq!(3.0, b.x);
/// assert_eq!(4.0, b.y);
/// 
/// assert_eq!(4.0, res.x);
/// assert_eq!(6.0, res.y);
/// ```
#[opimps::impl_ops(Add)]
#[inline]
fn add<T>(self: Vec2<T>, rhs: Vec2<T>) -> Vec2<T> where T: Add<Output = T> + Clone {
    let x = self.x.clone() + rhs.x.clone();
    let y = self.y.clone() + rhs.y.clone();
    Vec2 { x, y }
}


/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let v = Vec2::<f32>::new(1.0, 2.0);
///
/// let res: Vec2<f32> = (v + 3.0).into();
/// 
/// assert_eq!(4.0, res.x);
/// assert_eq!(5.0, res.y);
/// ```
#[opimps::impl_ops_rprim(Add)]
#[inline]
fn add<T>(self: Vec2<T>, rhs: T) -> Vec2<T> where T: Add<Output = T> + Clone {
    let x = self.x.clone() + rhs.clone();
    let y = self.y.clone() + rhs.clone();
    Vec2 { x, y }
}

/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let v = Vec2::<f32>::new(1.0, 2.0);
///
/// let res: Vec2<f32> = (3.0 + v).into();
/// 
/// assert_eq!(4.0, res.x);
/// assert_eq!(5.0, res.y);
/// ```
#[opimps::impl_ops_lprim(Add)]
#[inline]
fn add(self: f32, rhs: Vec2<f32>) -> Vec2<f32> {
    let x = self + rhs.x;
    let y = self + rhs.y;
    Vec2 { x, y }
}

/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let v = Vec2::<f64>::new(1.0, 2.0);
///
/// let res: Vec2<f64> = (3.0 + v).into();
/// 
/// assert_eq!(4.0, res.x);
/// assert_eq!(5.0, res.y);
/// ```
#[opimps::impl_ops_lprim(Add)]
#[inline]
fn add(self: f64, rhs: Vec2<f64>) -> Vec2<f64> {
    let x = self + rhs.x;
    let y = self + rhs.y;
    Vec2 { x, y }
}

/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let v = Vec2::<i32>::new(1, 2);
///
/// let res: Vec2<i32> = (3 + v).into();
/// 
/// assert_eq!(4, res.x);
/// assert_eq!(5, res.y);
/// ```
#[opimps::impl_ops_lprim(Add)]
#[inline]
fn add(self: i32, rhs: Vec2<i32>) -> Vec2<i32> {
    let x = self + rhs.x;
    let y = self + rhs.y;
    Vec2 { x, y }
}

/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let v = Vec2::<i64>::new(1, 2);
///
/// let res: Vec2<i64> = (3 + v).into();
/// 
/// assert_eq!(4, res.x);
/// assert_eq!(5, res.y);
/// ```
#[opimps::impl_ops_lprim(Add)]
#[inline]
fn add(self: i64, rhs: Vec2<i64>) -> Vec2<i64> {
    let x = self + rhs.x;
    let y = self + rhs.y;
    Vec2 { x, y }
}
