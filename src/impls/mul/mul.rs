use core::ops::Mul;
use crate::Vec2;

/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let mut a = Vec2::<f32>::new(1.5, 2.5);
/// let b = Vec2::<f32>::new(4.0, 2.0);
/// let c = &a * &b;
/// 
/// a.x = 2.0;
/// 
/// assert_eq!( 6.0, c.x);
/// assert_eq!( 5.0, c.y);
/// 
/// let c = a * b;
///
/// assert_eq!( 8.0, c.x);
/// assert_eq!( 5.0, c.y);
/// ```
#[opimps::impl_ops(Mul)]
#[inline]
fn mul<T>(self: Vec2<T>, rhs: Vec2<T>) -> Vec2<T> where T: Mul<Output = T> + Copy {
    Vec2 { x: self.x * rhs.x, y: self.y * rhs.y }
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let a = Vec2::<f32>::new(0.5, 2.5);
/// let b = Vec2::from(a * 2.0);
/// 
/// assert_eq!( 1.0, b.x);
/// assert_eq!( 5.0, b.y);
/// ```
#[opimps::impl_ops_rprim(Mul)]
#[inline]
fn mul<T>(self: Vec2<T>, rhs: T) -> Vec2<T> where T: Mul<Output = T> + Copy {
    Vec2 { x: self.x * rhs, y: self.y * rhs}
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let a = Vec2::<f32>::new(0.5, 2.5);
/// let b = 2.0 * a;
/// 
/// assert_eq!( 1.0, b.x);
/// assert_eq!( 5.0, b.y);
/// ```
#[opimps::impl_ops_lprim(Mul)]
#[inline]
fn mul(self: f32, rhs: Vec2<f32>) -> Vec2<f32> {
    Vec2 { x: self * rhs.x, y: self * rhs.y }
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let a = Vec2::<f64>::new(0.5, 2.5);
/// let b = 2.0 * a;
/// 
/// assert_eq!( 1.0, b.x);
/// assert_eq!( 5.0, b.y);
/// ```
#[opimps::impl_ops_lprim(Mul)]
#[inline]
fn mul(self: f64, rhs: Vec2<f64>) -> Vec2<f64> {
    Vec2 { x: self * rhs.x, y: self * rhs.y }
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let a = Vec2::<i32>::new(5, 2);
/// let b = 2 * a;
/// 
/// assert_eq!( 10, b.x);
/// assert_eq!( 4, b.y);
/// ```
#[opimps::impl_ops_lprim(Mul)]
#[inline]
fn mul(self: i32, rhs: Vec2<i32>) -> Vec2<i32> {
    Vec2 { x: self * rhs.x, y: self * rhs.y }
}

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let a = Vec2::<i64>::new(5, 2);
/// let b = 2 * a;
/// 
/// assert_eq!( 10, b.x);
/// assert_eq!( 4, b.y);
/// ```
#[opimps::impl_ops_lprim(Mul)]
#[inline]
fn mul(self: i64, rhs: Vec2<i64>) -> Vec2<i64> {
    Vec2 { x: self * rhs.x, y: self * rhs.y }
}
