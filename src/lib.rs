#![doc = include_str!("../README.md")]

mod vec2;
pub use vec2::*;

mod impls;

pub mod ops;
