use crate::{Vec2, ops::Norm};

/// ```
/// use gfxmath_vec2::ops::Norm;
/// use gfxmath_vec2::Vec2;
/// 
/// let a = Vec2::<f32>::new(3.0, 4.0);
/// let an = a.norm().unwrap();
/// 
/// assert_eq!(3.0/5.0, an.x);
/// assert_eq!(4.0/5.0, an.y);
/// ```
#[opimps::impl_uni_ops(Norm)]
#[inline]
fn norm(self: Vec2<f32>) -> Option<Vec2<f32>> {
    let l = (self.x * self.x + self.y * self.y).sqrt();
    if l == 0.0 { return None; }
    Some(Vec2 { x: self.x / l, y: self.y / l })
}

/// ```
/// use gfxmath_vec2::ops::Norm;
/// use gfxmath_vec2::Vec2;
/// 
/// let a = Vec2::<f64>::new(3.0, 4.0);
/// let an = a.norm().unwrap();
/// 
/// assert_eq!(3.0/5.0, an.x);
/// assert_eq!(4.0/5.0, an.y);
/// ```
#[opimps::impl_uni_ops(Norm)]
#[inline]
fn norm(self: Vec2<f64>) -> Option<Vec2<f64>> {
    let l = (self.x * self.x + self.y * self.y).sqrt();
    if l == 0.0 { return None; }
    Some(Vec2 { x: self.x / l, y: self.y / l })
}
