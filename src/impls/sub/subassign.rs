use core::ops::SubAssign;
use crate::Vec2;

///
/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let mut v1 = Vec2::new(1.0, 3.0);
/// let v2 = Vec2::new(2.0, 5.0);
/// v1 -= v2;
/// 
/// assert_eq!(-1.0, v1.x);
/// assert_eq!(-2.0, v1.y);
/// 
/// let mut v1 = Vec2::new(9.0, 5.0);
/// let v2 = Vec2::new(3.5, 2.0);
/// 
/// *(&mut v1) -= v2;
/// 
/// assert_eq!(5.5, v1.x);
/// assert_eq!(3.0, v1.y);
/// ```
#[opimps::impl_ops_assign(SubAssign)]
#[inline]
fn sub_assign<T>(self: Vec2<T>, rhs: Vec2<T>) where T: SubAssign<T> + Copy {
    self.x -= rhs.x;
    self.y -= rhs.y;
}

///
/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let mut v1 = Vec2::new(1.0, 3.0);
/// let v2 = (2.0, 5.0);
/// v1 -= v2;
/// 
/// assert_eq!(-1.0, v1.x);
/// assert_eq!(-2.0, v1.y);
/// 
/// let mut v1 = Vec2::new(9.0, 5.0);
/// let v2 = (3.5, 2.0);
/// 
/// *(&mut v1) -= v2;
/// 
/// assert_eq!(5.5, v1.x);
/// assert_eq!(3.0, v1.y);
/// ```
impl <T> SubAssign<(T, T)> for Vec2<T> where T: SubAssign {
    #[inline]
    fn sub_assign(&mut self, rhs: (T, T)) {
        self.x -= rhs.0;
        self.y -= rhs.1;
    }
}


///
/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let mut v1 = Vec2::new(1.0, 3.0);
/// v1 -= 2.0;
/// 
/// assert_eq!(-1.0, v1.x);
/// assert_eq!( 1.0, v1.y);
/// ```
#[opimps::impl_op_assign(SubAssign)]
#[inline]
fn sub_assign<T>(self: Vec2<T>, rhs: T) where T: SubAssign<T> + Copy {
    self.x -= rhs;
    self.y -= rhs;
}
