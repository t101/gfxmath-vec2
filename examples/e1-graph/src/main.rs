use std::time::Instant;

#[path = "lib/lib.rs"]
mod lib;
use lib::{init, shaderprogram::ShaderProgram};

use gfxmath_vec2::{Vec2, vec2};
use std::mem::size_of;

use sdl2::{event::Event, keyboard::Keycode};

/// Calculates the sinusoid for a given angle, displaying results from
/// -2pi to 2pi.
/// 
/// This function is for demo purposes. It is more efficient to calculate
/// the sinusoid in the shader.
fn calc_data(angle: f32, resolution: i32) -> Vec<Vec2<f32>> {
    let mut data = Vec::new();
    for i in (0..=(resolution * 2)).into_iter() {
        let x = (i as f32 / resolution as f32) * (std::f32::consts::PI * 2.0);
        let y = (angle + x).sin();
        
        let vx = (i as f32 / resolution as f32) - 1.0;
        data.push(vec2!(vx, y * 0.5));
    }
    data
}

fn main() {    
    let (sdl_context, _gl_ctx, window) = init::init("Waves");
    let mut event_pump = sdl_context.event_pump().unwrap();
    let shader = init::get_shader();
    let (vao, vbo) = init::init_buffers();
    let obj = Obj { vao, vbo };

    let mut last_time = Instant::now();
    let mut angle: f32 = 0.0;
    let mut shift_down = false;
    let mut resolution = 5;

    'render: loop {
        let current_time = Instant::now();
        let duration = current_time - last_time;
        last_time = current_time;
    
        if let Some(_) = process_input(&mut event_pump, &mut shift_down, &mut resolution) { break 'render; };
        let dwn = shift_down.clone();
        let angle = &mut angle as *mut f32;

        obj.draw(&shader, &||-> Vec<Vec2<f32>> {
            unsafe {
                let multiplier = if dwn { 2.0 } else { 1.0 };
                *angle += multiplier * (duration.as_millis() as f32) / 1000.0 * std::f32::consts::PI;
                calc_data(*angle, resolution)
            }
        });

        window.gl_swap_window();
        std::thread::sleep(::std::time::Duration::from_millis(1));
    }
}

fn process_input(event_pump: &mut sdl2::EventPump, shift_down: &mut bool, resolution: &mut i32) -> Option<()> {
    for event in event_pump.poll_iter() {
        match event {
            Event::Quit {..} | Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                return Some(())
            },
            Event::KeyDown { keycode: Some(Keycode::LShift), .. } => {
                *shift_down = true;
            }
            Event::KeyDown { keycode: Some(Keycode::Up), repeat: false, .. } => {
                *resolution += 1;
            }
            Event::KeyDown { keycode: Some(Keycode::Down), repeat: false, .. } => {
                *resolution -= 1;
            }

            _ => { *shift_down = false; }
        }
    }
    None
}

trait Drawable<T> {
    fn draw(&self, shader: &ShaderProgram, f: &dyn Fn() -> T);
}

struct Obj {
    vao: u32,
    vbo: u32
}

impl Drawable<Vec<Vec2<f32>>> for Obj {
    fn draw(&self, shader: &ShaderProgram, f: &dyn Fn() -> Vec<Vec2<f32>>) {
        unsafe {
            gl::ClearColor(0.0, 0.0, 0.0, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT);
    
            shader.activate();
            gl::BindVertexArray(self.vao);
            
            let data = f();
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BufferData(gl::ARRAY_BUFFER, (size_of::<Vec2<f32>>() * data.len()) as gl::types::GLsizeiptr, data.as_ptr() as *const std::ffi::c_void, gl::DYNAMIC_DRAW);

            gl::DrawArrays(gl::LINE_STRIP, 0, data.len() as gl::types::GLsizei);
        }
    }
}
