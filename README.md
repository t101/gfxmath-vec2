# gfxmath-vec2

[![Version](https://img.shields.io/crates/v/gfxmath-vec2)](https://crates.io/crates/gfxmath-vec2)
[![Downloads](https://img.shields.io/crates/d/gfxmath-vec2)](https://crates.io/crates/gfxmath-vec2)
[![Issues](https://img.shields.io/gitlab/issues/open/t101/gfxmath-vec2)](https://gitlab.com/t101/gfxmath-vec2/-/issues)

A simple 2D math library.

- [gfxmath-vec2](#gfxmath-vec2)
  - [Vec2](#vec2)
  - [Operators](#operators)
- [Known Limitations](#known-limitations)
  - [Left-Hand Primitives](#left-hand-primitives)
  - [Hash Implementation](#hash-implementation)
- [Showcase](#showcase)
- [License](#license)

## Vec2
There are multiple ways we can create a Vec2 object.

```rust
use gfxmath_vec2::Vec2;

// Standard 'new' semantics.
let v = Vec2::<f32>::new(3.0, 4.0);

// Using macro 'vec2!'
use gfxmath_vec2::vec2;
let v: Vec2<f32> = vec2!(3.0, 4.0);

// Using 'into' from a tuple.
let v: Vec2<f32> = (3.0, 4.0).into();

// Using 'into' from a borrowed tuple
let t = (3.0, 4.0);
let v: Vec2<f32> = (&t).into();

```

We can also create tuples from Vec2 objects.

```rust
use gfxmath_vec2::Vec2;

// With owned Vec2 object
let v = Vec2::<f32>::new(3.0, 4.0);
let t: (f32, f32) = v.into();

// With borrowed Vec2 object
let v = Vec2::<f32>::new(3.0, 4.0);
let t: (f32, f32) = (&v).into();
```

## Operators
Common mathematical operators are implemented for Vec2.

* `+` , `+=`
* `-` , `-=`
* `*` , `*=`
* `/` , `/=`

```rust
// Reminder: Rust automatically infers floats to `f64` and integers to `i64`.
use gfxmath_vec2::Vec2;

let v1 = Vec2::new(3.0, 9.0);
let v2 = Vec2::new(4.0, 5.0);

let res = v1 + v2;

assert_eq!(7.0, res.x);
assert_eq!(14.0, res.y);
```

# Known Limitations

## Left-Hand Primitives
One caveat is that operators where the Left-Hand side is a primitive has limited support. This is due to restrictions for trait implementations. `impl <T> Add<Vec2<T>> for T` is illegal within Rust syntax (due to the trailing `T`) because the implementation must be for a known type for non-local traits and types. Since `Add` is from the `core` package and `T` (the type to be implemented for) is not derived from a local trait, this is not possible.

At the time of this writing, primitives that work on the Left-Hand Side for common operators are `f32`, `f64`, `i32`, and `i64`.

```rust
use gfxmath_vec2::Vec2;

// Works
let lhs: f32 = 4.0;
let rhs = Vec2::<f32>::new(3.0, 4.0);
let res = lhs + rhs;
```

```rust ignore
// NOT SUPPORTED!!
let lhs: u32 = 4;
let rhs = Vec2::<u32>::new(3, 4);
let res = lhs + rhs;
```

## Hash Implementation
Currently, Hash implementations are limited for the following types:
- `f32`
- `f64`
- `i32`
- `i64`
- `u32`
- `u64`

# Showcase
<span title="Wave animation with Vec2">
<img src="https://gitlab.com/t101/gfxmath-vec2/raw/master/images/waves.gif" alt="drawing" width="300"/>
</span>

# License
Apache 2.0

