use core::ops::DivAssign;
use crate::Vec2;

///
/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let mut v1 = Vec2::new(1.0, 3.0);
/// let v2 = Vec2::new(2.0, 5.0);
/// v1 /= v2;
/// 
/// assert_eq!(0.5, v1.x);
/// assert_eq!(0.6, v1.y);
/// 
/// let mut v1 = Vec2::new(9.0, 5.0);
/// let v2 = Vec2::new(2.5, 2.0);
/// 
/// *(&mut v1) /= v2;
/// 
/// assert_eq!(3.6, v1.x);
/// assert_eq!(2.5, v1.y);
/// ```
#[opimps::impl_ops_assign(DivAssign)]
#[inline]
fn div_assign<T>(self: Vec2<T>, rhs: Vec2<T>) where T: DivAssign<T> + Copy {
    self.x /= rhs.x;
    self.y /= rhs.y;
}

///
/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let mut v1 = Vec2::new(1.0, 3.0);
/// let v2 = (2.0, 5.0);
/// v1 /= v2;
/// 
/// assert_eq!(0.5, v1.x);
/// assert_eq!(0.6, v1.y);
/// 
/// let mut v1 = Vec2::new(9.0, 5.0);
/// let v2 = (2.5, 2.0);
/// 
/// *(&mut v1) /= v2;
/// 
/// assert_eq!(3.6, v1.x);
/// assert_eq!(2.5, v1.y);
/// ```
impl <T> DivAssign<(T, T)> for Vec2<T> where T: DivAssign {
    #[inline]
    fn div_assign(&mut self, rhs: (T, T)) {
        self.x /= rhs.0;
        self.y /= rhs.1;
    }
}

///
/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let mut v1 = Vec2::new(9.0, 5.0);
/// 
/// v1 /= 2.0;
/// 
/// assert_eq!(4.5, v1.x);
/// assert_eq!(2.5, v1.y);
/// ```
#[opimps::impl_op_assign(DivAssign)]
#[inline]
fn div_assign<T>(self: Vec2<T>, rhs: T) where T: DivAssign<T> + Copy {
    self.x /= rhs;
    self.y /= rhs;
}
