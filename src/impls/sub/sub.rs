use core::ops::Sub;
use crate::Vec2;

/// 
/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let mut a = Vec2::<f32>::new(0.5, 2.5);
/// let b = Vec2::<f32>::new(1.5, 2.5);
/// let c = Vec2::from(&a - &b);
/// 
/// a.x = 1.0;
/// 
/// assert_eq!(-1.0, c.x);
/// assert_eq!( 0.0, c.y);
/// 
/// let c2 = Vec2::from(a - b);
///
/// assert_eq!(-0.5, c2.x);
/// assert_eq!( 0.0, c2.y);
/// ```
#[opimps::impl_ops(Sub)]
#[inline]
fn sub<T: Sub<Output = T> + Copy>(self: Vec2<T>, rhs: Vec2<T>) -> Vec2<T> {
    let x = self.x - rhs.x;
    let y = self.y - rhs.y;
    Vec2 { x, y }
}

/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let v = Vec2::new(1.0, 2.0);
///
/// let res: Vec2<f32> = (v - 3.0).into();
/// 
/// assert_eq!(-2.0, res.x);
/// assert_eq!(-1.0, res.y);
/// ```
#[opimps::impl_ops_rprim(Sub)]
#[inline]
fn sub<T>(self: Vec2<T>, rhs: T) -> Vec2<T> where T: Sub<Output = T> + Copy {
    Vec2 { x: self.x - rhs, y: self.y - rhs }
}

/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let v = Vec2::<f32>::new(1.0, 2.0);
///
/// let res = (3.0 - v);
/// 
/// assert_eq!( 2.0, res.x);
/// assert_eq!( 1.0, res.y);
/// ```
#[opimps::impl_ops_lprim(Sub)]
#[inline]
fn sub(self: f32, rhs: Vec2<f32>) -> Vec2<f32> {
    let x = self - rhs.x;
    let y = self - rhs.y;
    Vec2 { x, y }
}

/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let v = Vec2::<f32>::new(1.0, 2.0);
///
/// let res = (3.0 - v);
/// 
/// assert_eq!( 2.0, res.x);
/// assert_eq!( 1.0, res.y);
/// ```
#[opimps::impl_ops_lprim(Sub)]
#[inline]
fn sub(self: f64, rhs: Vec2<f64>) -> Vec2<f64> {
    let x = self - rhs.x;
    let y = self - rhs.y;
    Vec2 { x, y }
}

/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let v = Vec2::<i32>::new(1, 2);
///
/// let res = (3 - v);
/// 
/// assert_eq!( 2, res.x);
/// assert_eq!( 1, res.y);
/// ```
#[opimps::impl_ops_lprim(Sub)]
#[inline]
fn sub(self: i32, rhs: Vec2<i32>) -> Vec2<i32> {
    let x = self - rhs.x;
    let y = self - rhs.y;
    Vec2 { x, y }
}

/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let v = Vec2::<i64>::new(1, 2);
///
/// let res = (3 - v);
/// 
/// assert_eq!( 2, res.x);
/// assert_eq!( 1, res.y);
/// ```
#[opimps::impl_ops_lprim(Sub)]
#[inline]
fn sub(self: i64, rhs: Vec2<i64>) -> Vec2<i64> {
    let x = self - rhs.x;
    let y = self - rhs.y;
    Vec2 { x, y }
}
