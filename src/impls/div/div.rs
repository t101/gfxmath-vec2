use core::ops::Div;
use crate::Vec2;

/// Scalar division with vector
/// 
/// ```
/// use gfxmath_vec2::Vec2;
/// 
/// let a = Vec2::<f32>::new(0.5, 2.5);
/// let b = a / 2.0;
/// 
/// assert_eq!( 0.25, b.x);
/// assert_eq!( 1.25, b.y);
/// ```
#[opimps::impl_ops_rprim(Div)]
#[inline]
fn div<T>(self: Vec2<T>, rhs: T) -> Vec2<T> where T: Div<Output = T> + Copy {
    Vec2 { x: self.x / rhs, y: self.y / rhs }
}
