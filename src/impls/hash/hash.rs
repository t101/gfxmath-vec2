
use core::hash::Hash;
use crate::Vec2;

const HASH_PRIME: u32 = 31;

//////////     FLOATS    ////////////
impl Hash for Vec2<f32> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let xu = unsafe { *(&self.x as *const _ as *const u32) };
        let yu = unsafe { *(&self.y as *const _ as *const u32) };

        xu.hash(state);
        HASH_PRIME.hash(state);
        yu.hash(state);
        HASH_PRIME.hash(state);
    }
}

impl Hash for Vec2<f64> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let xu = unsafe { *(&self.x as *const _ as *const u64) };
        let yu = unsafe { *(&self.y as *const _ as *const u64) };

        xu.hash(state);
        HASH_PRIME.hash(state);
        yu.hash(state);
        HASH_PRIME.hash(state);
    }
}
// END OF -- FLOATS    /////////////


///////////// SIGNED INTEGERS ////////////
impl Hash for Vec2<i32> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.x.hash(state);
        HASH_PRIME.hash(state);
        self.y.hash(state);
        HASH_PRIME.hash(state);
    }
}

impl Hash for Vec2<i64> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.x.hash(state);
        HASH_PRIME.hash(state);
        self.y.hash(state);
        HASH_PRIME.hash(state);
    }
}
// END OF --  SIGNED INTEGERS ////////////




/////////// UNSIGNED INTEGERS ////////////
impl Hash for Vec2<u32> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.x.hash(state);
        HASH_PRIME.hash(state);
        self.y.hash(state);
        HASH_PRIME.hash(state);
    }
}

impl Hash for Vec2<u64> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.x.hash(state);
        HASH_PRIME.hash(state);
        self.y.hash(state);
        HASH_PRIME.hash(state);
    }
}
// END OF --UNSIGNED INTEGERS ////////////


#[cfg(test)]
mod tests {
    use std::collections::{HashSet};

    use crate::Vec2;

    #[test]
    fn test_hashset() {
        let mut set = HashSet::new();
        let v = Vec2::<f32>::new(2.0, 4.0);
        set.insert(v.clone());
        assert!(set.contains(&v));

        let mut set = HashSet::new();
        let v = Vec2::<f64>::new(2.0, 4.0);
        set.insert(v.clone());
        assert!(set.contains(&v));
    }

    #[test]
    fn test_hashset_integers() {
        let mut set = HashSet::new();
        let v = Vec2::<i32>::new(2, 4);
        set.insert(v.clone());
        assert!(set.contains(&v));

        let mut set = HashSet::new();
        let v = Vec2::<i64>::new(2, 4);
        set.insert(v.clone());
        assert!(set.contains(&v));
    }
}
