use core::ops::Neg;
use crate::Vec2;

/// Scalar multiplication with vector
/// 
/// ```
/// use gfxmath_vec2::Vec2;
///
/// let a = Vec2::<f32>::new(2.0, 6.0);
/// 
/// let res = -a;
///
/// assert_eq!(-2.0, res.x);
/// assert_eq!(-6.0, res.y);
/// ```
#[opimps::impl_uni_ops(Neg)]
#[inline]
fn neg<T>(self: Vec2<T>) -> Vec2<T> where T: Neg<Output = T> + Copy {
    Vec2 { x: -self.x, y: -self.y }
}
